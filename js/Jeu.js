class Jeu {
  constructor(size) {
    this.size = size;
    this.players = new Array(2);
    this.victories = new Array(2);
    this.board = new Array(size);
    this.isActive = true;
    this.emptyBoard();
    this.initPlayers();
    this.currentPlayer = 0;
    this.html = document.getElementById("root");
  }

  emptyBoard = () => {
    for (let i = 0; i < this.size; i++) {
      this.board[i] = Array.apply(null, Array(this.size)).map(function() {
        return "";
      });
    }
  };

  initPlayers = () => {
    this.players[0] = "J1 (X)";
    this.players[1] = "J2 (O)";

    this.victories[0] = 0;
    this.victories[1] = 0;

    document.getElementById("player-0").innerHTML = this.players[0];
    document.getElementById("player-1").innerHTML = this.players[1];
    this.displayVictories();
  };

  changeTurn = () => {
    document.getElementById(`player-${this.currentPlayer}`).className = "";
    this.currentPlayer = this.currentPlayer === 0 ? 1 : 0;
    document.getElementById(`player-${this.currentPlayer}`).className =
      "current-player";
  };

  displayBoard = () => {
    let table = "<table>";

    this.board.forEach((row, i) => {
      table += "<tr>";

      row.forEach((col, j) => {
        table +=
          `<td onclick='jeu.play(${i}, ${j})' id="${i}-${j}">` + col + `</td>`;
      });

      table += "</tr>";
    });

    table += "</table>";
    this.html.innerHTML = table;
  };

  displayVictories = () => {
    document.getElementById("victory-0").innerHTML =
      "Victoires : " + this.victories[0];
    document.getElementById("victory-1").innerHTML =
      "Victoires : " + this.victories[1];
  };

  updateBoard = () => {
    this.board.forEach((row, i) => {
      row.forEach((col, j) => {
        document.getElementById(`${i}-${j}`).innerHTML = col;
      });
    });
  };

  checkBLeftTRight = () => {
    const symbol = this.currentPlayer === 0 ? "X" : "O";

    let score = 0;
    for (let j = 0; j < this.size; j++) {
      const i = this.size - 1 - j;
      if (this.board[i][j] === symbol) {
        score++;
      }
    }
    if (score >= this.size) {
      this.winMessage();
      return true;
    }
  };

  checkBRightTLeft = () => {
    const symbol = this.currentPlayer === 0 ? "X" : "O";

    let score = 0;
    for (let i = 0; i < this.size; i++) {
      if (this.board[i][i] === symbol) {
        score++;
      }
    }
    if (score >= this.size) {
      this.winMessage();
      return true;
    }
  };

  checkVertical = () => {
    const symbol = this.currentPlayer === 0 ? "X" : "O";
    for (let j = 0; j < this.size; j++) {
      let score = 0;
      for (let i = 0; i < this.size; i++) {
        if (this.board[i][j] === symbol) {
          score++;
        }
      }
      if (score >= this.size) {
        this.winMessage();
        return true;
      }
    }
  };

  checkHorizontal = () => {
    const symbol = this.currentPlayer === 0 ? "X" : "O";

    for (let i = 0; i < this.size; i++) {
      let score = 0;
      for (let j = 0; j < this.size; j++) {
        if (this.board[i][j] === symbol) {
          score++;
        }
      }
      if (score >= this.size) {
        this.winMessage();
        return true;
      }
    }
  };

  checkWins = () => {
    if (
      this.checkHorizontal() ||
      this.checkVertical() ||
      this.checkBLeftTRight() ||
      this.checkBRightTLeft()
    ) {
      this.victories[this.currentPlayer]++;
      this.displayVictories();
      return true;
    }
  };

  displayMessage = (message, type) => {
    window.scrollTo({
      top: 0,
      left: 0,
      behavior: "smooth"
    });
    let alert = document.getElementById("message-alert");
    alert.className = "alert alert-" + type;
    alert.style.visibility = "visible";
    alert.style.opacity = 1;
    alert.firstChild.nodeValue = message;
    setTimeout(() => {
      alert.style.visibility = "hidden";
      alert.style.opacity = 0;
    }, 3000);
  };

  winMessage = () => {
    this.displayMessage(
      `Victoire du joueur ${this.players[this.currentPlayer]} !`,
      "success"
    );
  };

  errorMessage = error => {
    this.displayMessage(error, "danger");
  };

  verifPlein = () => {
    let nbCases = 0;
    this.board.forEach(row => {
      row.forEach(col => {
        if (col.length > 0) {
          nbCases++;
        }
      });
    });
    if (nbCases === this.size * this.size) {
      this.errorMessage("Jeu rempli, match nul !");
      return true;
    } else {
      return false;
    }
  };

  play = (i, j) => {
    // Play only if game is active
    if (this.isActive) {
      if (this.board[i][j] === "") {
        this.board[i][j] = this.currentPlayer === 0 ? "X" : "O";
        this.updateBoard();
        if (this.checkWins() || this.verifPlein()) {
          this.isActive = false;
          setTimeout(() => {
            this.emptyBoard();
            this.updateBoard();
            this.isActive = true;
          }, 1000);
        } else {
          this.changeTurn();
        }
      } else {
        this.errorMessage("Case déjà utilisée. Recommencez !");
      }
    }
  };
}
