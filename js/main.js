let jeu = new Jeu(3);
jeu.displayBoard();

// Generates a new board
const generate = () => {
  const size = parseInt(document.getElementById("size").value);

  // Checking that the size is correct
  if (size > 0 && !isNaN(size)) {
    if (jeu) {
      jeu.board = new Array(size);
      jeu.size = size;
      jeu.emptyBoard();
    } else {
      jeu = new Jeu(size);
    }
    jeu.displayBoard();
  } else {
    alert("Saisissez un nombre valide (> 0)");
  }
};

// Generate new game board when pressing
// enter in input for board size
document.getElementById("size").addEventListener("keyup", event => {
  if (event.key !== "Enter") return;
  generate();
  event.preventDefault();
});

// Function to execute when clicking on player names
const editName = i => {
  // Getting the h3 w/ name
  let player = document.getElementById("player-" + i);
  const className = i === jeu.currentPlayer ? "current-player" : "";
  // Changing h3 into text input to change name
  player.outerHTML = `<input type="text" id="player-${i}" value="${
    jeu.players[i]
  }">`;
  // Getting new DOM element and focusing it
  player = document.getElementById("player-" + i);
  player.focus();
  // When losing focus or pressing enter,
  // apply changes to player name and change
  // it back into a h3
  player.addEventListener("blur", event => {
    jeu.players[i] = player.value;
    player.outerHTML = `<h3 id="player-${i}" onclick="editName(${i})" class="${className}">${
      jeu.players[i]
    }</h3>`;
    event.preventDefault();
  });
  // Pressing enter blurs the input
  player.addEventListener("keyup", event => {
    if (event.key !== "Enter") return;
    player.blur();
    event.preventDefault();
  });
  event.preventDefault();
};

const resetScores = () => {
  jeu.victories[0] = 0;
  jeu.victories[1] = 0;
  jeu.displayVictories();
};

const closeAlert = id => {
  let alert = document.getElementById(id);
  alert.style.visibility = "hidden";
  alert.style.opacity = 0;
};
